


function iterateRecords(data){

	
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 6,
		center: {lat: -23.451853, lng: 144.266907}
	});
	
	var markers = [];
	var centerAt = {lat: -23.451853, lng: 144.266907};
	var zoomAt = 6;
	
	//Function of region selection
		//when region is changed, refresh the map and center to that region
	document.getElementById("selectYear").onchange = function(){
		
		//get the lat and lng of that region
		var targetRegion = $( "select" ).val();
		if(targetRegion == 'Queensland'){
			zoomAt = 6 ;
			centerAt = {lat: -23.451853, lng: 144.266907};
		} else if (targetRegion == 'Far North Queensland'){
			zoomAt = 7 ;
			centerAt = {lat: -16.212565, lng: 143.129883};
		} else if (targetRegion == 'North Queensland'){
			zoomAt = 7 ;
			centerAt = {lat: -20.772230, lng: 144.785153};
		} else if (targetRegion == 'Mackay, Isaac and Whitsunday'){
			zoomAt = 7 ;
			centerAt = {lat: -21.410116, lng: 148.798828};
		} else if (targetRegion == 'Central Queensland'){
			zoomAt = 7 ;
			centerAt = {lat: -24.804686, lng: 145.942383};
		} else if (targetRegion == 'Wide Bay-Burnett'){
			zoomAt = 7 ;
			centerAt = {lat: -25.560282, lng: 152.006836};
		} else if (targetRegion == 'Darling Downs South West'){
			zoomAt = 7 ;
			centerAt = {lat: -27.564773, lng: 146.997070};
		} else if (targetRegion == 'South East Queensland'){
			zoomAt = 7 ;
			centerAt = {lat: -27.914825, lng: 152.973633};
		} 
		$('#map').empty();
		var map = new google.maps.Map(document.getElementById('map'), {
				zoom: zoomAt,
				center: centerAt
		});
		var locations = [];
		for (var i = 0; i < markers.length; i++) {
					var imageIcon = "mineIcon.png";
					//create an infoWindow
					var infoWindow = new google.maps.InfoWindow();
					
					//create markers
					locations[i] = new google.maps.Marker({
						position: {lat: markers[i][0], lng: markers[i][1]},
						map: map,
						icon: imageIcon
					});
					
					//add listener to markers
					locations[i].addListener('click', function() {
						
						//set position for infoWindow
						infoWindow.setPosition(this.getPosition());
						var index = locations.indexOf(this);
						//get the text of remark which belongs to that specific marker
						var remarkToDisplay = markers[index][2];
						var nameToDisplay = markers[index][3];
						infoWindow.setContent( '<p>' + nameToDisplay +'<hr>' + remarkToDisplay + '</p>' + '<a style="color:red;" href="exploreMore.html" target="_top">Explore More</a>'); 
						infoWindow.open(map);
					});
		}
	}
	
	//when year is changed, do something
		
		$(".years").click(function(){
			
			//get the value of chosen year
			var targetYear = $(this).attr('id');
			markers = [];
			
			//iterate through records and get lat, lng, remark then add it to the array
			$.each(data.result.records, function(recordKey, recordValue){
				var	recordDate = recordValue['Year'];
				var YesOrNo = recordDate.includes(targetYear);
				if(YesOrNo == true){
					var lat = recordValue['Latitude'].split(",")[0];
					var lng = recordValue['Latitude'].split(",")[1];
					var remark = recordValue['Remarks'];
					var nameOfMine = recordValue['Name Of Mine'];
					//check they have values
					if(lat&&lng&&remark&&nameOfMine){
						var marker = [parseFloat(lat),parseFloat(lng), remark, nameOfMine];
						markers.push(marker);
					}
				}
			});
			
			$('#map').empty();
			var map = new google.maps.Map(document.getElementById('map'), {
				zoom: zoomAt,
				center: centerAt
			});
			
			var locations = [];
			for (var i = 0; i < markers.length; i++) {
					var imageIcon = "mineIcon.png";
					//create an infoWindow
					var infoWindow = new google.maps.InfoWindow();
					
					//create markers
					locations[i] = new google.maps.Marker({
						position: {lat: markers[i][0], lng: markers[i][1]},
						map: map,
						icon: imageIcon
					});
					
					//add listener to markers
					locations[i].addListener('click', function() {
						
						//set position for infoWindow
						infoWindow.setPosition(this.getPosition());
						var index = locations.indexOf(this);
						//get the text of remark which belongs to that specific marker
						var remarkToDisplay = markers[index][2];
						var nameToDisplay = markers[index][3];
						infoWindow.setContent( '<p>' + nameToDisplay +'<hr>' + remarkToDisplay + '</p>' + '<a style="color:red;" href="exploreMore.html" target="_top">Explore More</a>'); 
						infoWindow.open(map);
					});
			}
		});
		
		//these years dont have any accident records
		$("#1882, #1940, #1941, #1942, #1943, #1944").click(function(){
			alert("There is no mining accident in this year, try another year.");
		});
}



	$(document).ready(function() {
					
					if (localStorage.getItem('slqData')) {
						data = localStorage.getItem('slqData');
						data = JSON.parse(data);
						var locations = iterateRecords(data);
						console.log('From localStorage');
					}else{
						var data = {
							resource_id: '63fd8050-0bab-4c04-b837-b2ce664077bf',
							limit:10000
						}

						$.ajax({
							url: 'https://data.gov.au/api/action/datastore_search',
							data: data,
							dataType: 'jsonp',
							cache: true,
							success: function(data) {
								iterateRecords(data);
								data = JSON.stringify(data);
								localStorage.setItem('slqData', data);
								console.log('From API');
							}
						});	
					}
			});