<!DOCTYPE html>
<html lang="en">
<head>
    <title>The Map of Mining Accidents in Queensland's History</title>
	<meta charset="utf-8" />
    <script src='js/jquery-3.2.1.min.js'></script>
    <link rel="stylesheet" href="slideoutMenu.css">
	<link rel="stylesheet" href="index.css">
</head>

<body>

	<div id="loader-wrapper">
			<div id="loader"></div>
			<div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>

	</div>


<div id="loaderBox" class="loaderBox">
    <div class="loadAnim">
        <div class="loadAnim1"></div>
        <div class="loadAnim2"></div>
        <div class="loadAnim3"></div>
    </div>
</div>

<nav id="menu" class="menu">
    <header class="menu-header">
        <span class="menu-header-title">DECO1800</span>
    </header>

    <section class="menu-section">
        <h3 class="menu-section-title">LINKS</h3>
        <ul class="menu-section-list">
			<li><a href="exploreMore.html" target="_top">Explore More</a></li>
            <li><a href="http://data.gov.au/" target="_top">Find Open Data Here</a></li>
            <li><a href="https://developers.google.com/maps/?hl=zh-cn" target="_top">How to Use Google Map API</a></li>   
			<li><a href="https://en.wikipedia.org/wiki/Mining_accident" target="_top">Wikipedia for Mining Accidents</a></li>  
			<li><a href="http://data.gov.au/dataset/41701e1a-b1cc-4d8b-98d1-c0dcbffc2795/resource/63fd8050-0bab-4c04-b837-b2ce664077bf/download/slqqldminingaccidents.csv" target="_top">Download Our Data! It's FREE!</a></li>			
        </ul>
    </section>
</nav>

<div id="container" class="container">
    <div id="header">
        <table>
            <tr>
                <td>
                    <button class="btn-burger">Menu</button>
                </td>
                <td id="title">The Map of Mining Accidents in Queensland's History</td>
                <td id='year'>
                    <select name="year" id="selectYear">
                        <option>Queensland</option>
                        <option>South East Queensland</option>
                        <option>Darling Downs South West</option>
                        <option>Wide Bay-Burnett</option>
                        <option>Central Queensland</option>
                        <option>Mackay, Isaac and Whitsunday</option>
                        <option>North Queensland</option>
                        <option>Far North Queensland</option>
                    </select>
                </td>
            </tr>
        </table>
    </div>

    <div id="map"></div>

    <footer>
        <div class="time_line" style="width:100%;">
            <ol>
                <li><a class="years" id="1882" style="left:1.538%;">1882</a></li>
                <li><a class="years" id="1883" style="left:3.077%;">1883</a></li>
                <li><a class="years" id="1884" style="left:4.615%;">1884</a></li>
                <li><a class="years" id="1885" style="left:6.1538%;">1885</a></li>
                <li><a class="years" id="1886" style="left:7.6923%;">1886</a></li>
                <li><a class="years" id="1887" style="left:9.2307%;">1887</a></li>
                <li><a class="years" id="1888" style="left:10.7692%;">1888</a></li>
                <li><a class="years" id="1889" style="left:12.307%;">1889</a></li>
                <li><a class="years" id="1890" style="left:13.8462%;">1890</a></li>
                <li><a class="years" id="1891" style="left:15.3846%;">1891</a></li>
                <li><a class="years" id="1892" style="left:16.9231%;">1892</a></li>
                <li><a class="years" id="1893" style="left:18.4615%;">1893</a></li>
                <li><a class="years" id="1894" style="left:20%;">1894</a></li>
                <li><a class="years" id="1895" style="left:21.5383%;">1895</a></li>
                <li><a class="years" id="1896" style="left:23.0769%;">1896</a></li>
                <li><a class="years" id="1897" style="left:24.6154%;">1897</a></li>
                <li><a class="years" id="1898" style="left:26.1538%;">1898</a></li>
                <li><a class="years" id="1899" style="left:27.6923%;">1899</a></li>
                <li><a class="years" id="1900" style="left:29.2307%;">1900</a></li>
                <li><a class="years" id="1901" style="left:30.76923%;">1901</a></li>
                <li><a class="years" id="1902" style="left:32.3077%;">1902</a></li>
                <li><a class="years" id="1903" style="left:33.8461%;">1903</a></li>
                <li><a class="years" id="1904" style="left:35.4%;">1904</a></li>
                <li><a class="years" id="1905" style="left:36.92307%;">1905</a></li>
                <li><a class="years" id="1906" style="left:38.46153%;">1906</a></li>
                <li><a class="years" id="1907" style="left:40%;">1907</a></li>
                <li><a class="years" id="1908" style="left:41.5384%;">1908</a></li>
                <li><a class="years" id="1909" style="left:43.0769%;">1909</a></li>
                <li><a class="years" id="1910" style="left:44.61538%;">1910</a></li>
                <li><a class="years" id="1911" style="left:46.15384%;">1911</a></li>
                <li><a class="years" id="1912" style="left:47.692307%;">1912</a></li>
                <li><a class="years" id="1913" style="left:49.2307%;">1913</a></li>
                <li><a class="years" id="1914" style="left:50.76923%;">1914</a></li>
                <li><a class="years" id="1915" style="left:52.30769229%;">1915</a></li>
                <li><a class="years" id="1916" style="left:53.84615%;">1916</a></li>
                <li><a class="years" id="1917" style="left:55.384615%;">1917</a></li>
                <li><a class="years" id="1918" style="left:56.923076%;">1918</a></li>
                <li><a class="years" id="1919" style="left:58.461538%;">1919</a></li>
                <li><a class="years" id="1920" style="left:59.99999%;">1920</a></li>
                <li><a class="years" id="1921" style="left:61.53846152%;">1921</a></li>
                <li><a class="years" id="1922" style="left:63.07692306%;">1922</a></li>
                <li><a class="years" id="1923" style="left:64.6153846%;">1923</a></li>
                <li><a class="years" id="1924" style="left:66.15384613%;">1924</a></li>
                <li><a class="years" id="1925" style="left:67.69230767%;">1925</a></li>
                <li><a class="years" id="1926" style="left:69.230769%;">1926</a></li>
                <li><a class="years" id="1927" style="left:70.76923075%;">1927</a></li>
                <li><a class="years" id="1928" style="left:72.30769229%;">1928</a></li>
                <li><a class="years" id="1929" style="left:73.84615382%;">1929</a></li>
                <li><a class="years" id="1930" style="left:75.38461536%;">1930</a></li>
                <li><a class="years" id="1931" style="left:76.9230769%;">1931</a></li>
                <li><a class="years" id="1932" style="left:78.46153844%;">1932</a></li>
                <li><a class="years" id="1933" style="left:79.9999%;">1933</a></li>
                <li><a class="years" id="1934" style="left:81.53846151%;">1934</a></li>
                <li><a class="years" id="1935" style="left:83.07692305%;">1935</a></li>
                <li><a class="years" id="1936" style="left:84.61538459%;">1936</a></li>
                <li><a class="years" id="1937" style="left:86.15384613%;">1937</a></li>
                <li><a class="years" id="1938" style="left:87.69230767%;">1938</a></li>
                <li><a class="years" id="1939" style="left:89.2307692%;">1939</a></li>
                <li><a class="years" id="1940" style="left:90.76923074%;">1940</a></li>
                <li><a class="years" id="1941" style="left:92.30769228%;">1941</a></li>
                <li><a class="years" id="1942" style="left:93.84615382%;">1942</a></li>
                <li><a class="years" id="1943" style="left:95.38461536%;">1943</a></li>
                <li><a class="years" id="1944" style="left:96.92307689%;">1944</a></li>
            </ol>
        </div>
    </footer>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAvnpcb4nEPYP2qteuW77fOUI7rR2vMlq0&callback=initMap">
    </script>

    <script src="js/data.js"></script>

    <script src="js/slideout.min.js"></script>
    <script>

        var slideout = new Slideout({
            'panel': document.getElementById('container'),
            'menu': document.getElementById('menu'),
            'padding': 256,
            'tolerance': 70
        });

        window.onload = function () {
            document.querySelector('.btn-burger').addEventListener('click', function () {
                slideout.toggle();
            });

        };
    </script>
</div>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
	<script src="js/loading.js"></script>
</body>

</html>
